package br.com.calculadora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculadoraTest {


    private Calculadora calculadora;

    @BeforeEach             // Executado antes de cada método da classe
    public void setUp() {
        calculadora = new Calculadora();
    }


    @Test
    public void testaASomaDeDoisNumerosInteiros() {
        int resultado = calculadora.soma(1, 2);
        Assertions.assertEquals(3, resultado);
    }

    @Test
    public void testaASomaDeDoisNumeroFlutuantes() {
        double resultado = calculadora.soma(2.3, 3.4);
        Assertions.assertEquals(5.7, resultado);
    }

    @Test
    public void testaAMultiplicacaoDeDoisNumerosInteirosENegativos() {
        int resultado = calculadora.multiplicacao(-2, 3);
        Assertions.assertEquals(-6, resultado);
    }

    @Test
    public void testaAMultiplicacaoDeDoisNumerosFlutuantes() {
        double resultado = calculadora.multiplicacao(2.2, 3.2);
        Assertions.assertEquals(7.04, resultado);
    }

    @Test
    public void testaADivisaoDeDoisNumerosInteirosENegativosFlutuantes() {
        double resultado = calculadora.divisao(2712423.6542, 2.47);
        Assertions.assertEquals(1098147.228, resultado);
    }


}
